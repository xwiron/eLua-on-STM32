// Generic platform configuration file

#ifndef __PLATFORM_CONF_H__
#define __PLATFORM_CONF_H__

#define LUA_PLATFORM_LIBS_REG \
  {LUA_LOADLIBNAME,	luaopen_package },\
  {LUA_TABLIBNAME,	luaopen_table },\
  {LUA_IOLIBNAME,	luaopen_io },\
  {LUA_OSLIBNAME, luaopen_os},\
  {LUA_STRLIBNAME,	luaopen_string },\
  {LUA_MATHLIBNAME,	luaopen_math },\
  {LUA_DBLIBNAME,	luaopen_debug }
  
#endif

