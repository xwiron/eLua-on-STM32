#include <stdio.h>
#include "stm32f10x.h"

int clock(void)
{
	return(NULL);
}

int system(const char *system)
{
	return(NULL);
}

int time(int *timer)
{
	return(NULL);
}

int sendchar(int ch)
{
  /* Loop until the end of transmission */
  while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
  USART_SendData(USART1, (uint8_t) ch);	
  return ch;
}

int getkey(void)
{
	/* Loop until the USARTz Receive Data Register is not empty */
	while(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);
	return USART_ReceiveData(USART1);  
}

void exit(int status)
{
	while(1);
}


#ifdef __MICROLIB

int fputc(int ch, FILE *f)
{
	if(ch == '\n')
	{
		sendchar(ch);
		ch = '\r';
	}
	return(sendchar(ch));
}

int fgetc(FILE *f)
{
	return(sendchar(getkey()));
}

char *fgets(char *buf, int len, FILE *mode)
{
	static unsigned char pos;

	for(pos = len;pos > 0;pos--) buf[pos] = '\0';
	do {
        buf[pos++] = getkey();
        if(buf[pos - 1] == '\b') 
        {
            if(pos > 1)
            {
                sendchar('\b');
                sendchar(' ');
                sendchar('\b'); 
                pos -= 2;   
            }
            else if(pos > 0) pos--;
        }
        else if(buf[pos - 1] == '\4') buf[pos++] = '\r';
		else sendchar(buf[pos - 1]);
    } while(buf[pos - 1] != '\r');
	sendchar('\n');
    return(buf); 
}

char *getenv(const char *name)
{
	return(NULL);
}

int ungetc(int c, FILE *stream)
{
	return(NULL);
}

int __backspace(FILE *stream)
{
	return(NULL);
}

#else

int remove(const char *name)
{
	return(NULL);
}

int rename(const char *old_name, const char *new_name)
{
	return(NULL);
}

#endif

#pragma import(__use_no_semihosting_swi)

int _sys_open(const char *name, int openmode)
{	
   	return(_IOFBF);
}

int _sys_close(int fh)
{
	return(NULL);
}

int _sys_write(int fh, const unsigned char *buf, unsigned long len, int mode)
{
    static unsigned char pos;
	for(pos = 0;pos < len;pos++) 
    {
        sendchar(buf[pos]);  
        if(buf[pos] == '\n') sendchar('\r');
    }    
    return(NULL);   
}

int _sys_read(int fh, unsigned char *buf, unsigned long len, int mode)
{
	static unsigned char pos;

	for(pos = len;pos > 0;pos--) buf[pos] = '\0';
	do {
        buf[pos++] = getkey();
        if(buf[pos - 1] == '\b') 
        {
            if(pos > 1)
            {
                sendchar('\b');
                sendchar(' ');
                sendchar('\b'); 
                pos -= 2;   
            }
            else if(pos > 0) pos--;
        }
		else if(buf[pos - 1] == '\4') buf[pos++] = '\r';
        else sendchar(buf[pos - 1]);
    } while(buf[pos - 1] != '\r');
	sendchar('\n');
    return(NULL); 
}

void _ttywrch(int ch)
{
	sendchar(ch);
}

int _sys_istty(int fh)
{
   	return(1);
}

int _sys_seek(int fh, long pos)
{
	return(EOF);
}

int _sys_ensure(int fh)
{
	return(NULL);
}

long _sys_flen(int fh)
{
	return(NULL);
}

int _sys_tmpnam(char *name, int sig, unsigned maxlen)
{
	return(NULL);
}

void _sys_exit(int return_code)
{
	while(1);
}
