// Lua shell 

#ifndef __SHELL_H__
#define __SHELL_H__

#include <stdio.h>
#define SHELL_PROMPT        "Lua# "
#define SHELL_ERRMSG        "Invalid command, type 'help' for help\n"
#define SHELL_MAXSIZE       STDIN_BUFSIZ + 1
#define SHELL_MAX_LUA_ARGS  8

int shell_init(void);
void shell_start(void);

#endif // #ifndef __SHELL_H__
